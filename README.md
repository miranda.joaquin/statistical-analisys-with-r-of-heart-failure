# Statistical analysis with R of heart failure

I deploy a combination of descriptive and inferential statistics to understand the cases and importantly, predict the survival probability given 12 variables. 

## Abstract
Myocardial infarctions and heart failures are responsible for the deaths of 17 million people around the world. Hence, it is very important to understand the key factors that lead to the causes of heart failure. In this study, I  focused on the survival analysis of 299 heart failure patients based on data released by Ahmad et al [1]. I deploy a combination of descriptive and inferential statistics to understand the cases and importantly, predict the survival probability given 12 variables. This was achieved by the graphical prediction of a nomogram. The results of statistical analysis show that serum creatinine, ejection fraction followed by age, and serum sodium are sufficient to predict the survival of heart failure patients.

[1] https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0181001

## Methods
The following method were deployed:

1. Basic descriptive statistics to explore data.
2. Cumulative proportion for selected variables.
3. Chi-square, Mann–Whitney U tests and Cox regression was used to model mortality.
4. Kaplan Meier plot was used to study the general pattern of survival.
5. Nomogram to assign the probability of death.


